﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionViewTest
{
    static class MovieStorage
    {
        public static List<Movie> GetMovies()
        {
            return new List<Movie>
            {
                new Movie()
                {
                    Title = "Star Wars: Episode I - The Phantom Menace",
                    ReleaseYear = 1999,
                    Rating = 6.5m
                },
                new Movie()
                {
                    Title = "Star Wars",
                    ReleaseYear = 1977,
                    Rating = 8.7m
                },
                new Movie()
                {
                    Title = "Star Wars: Episode V - The Empire Strikes Back",
                    ReleaseYear = 1980,
                    Rating = 8.8m
                },
                new Movie()
                {
                    Title = "Star Wars: Episode II - Attack of the Clones",
                    ReleaseYear = 2002,
                    Rating = 6.7m
                },
                new Movie()
                {
                    Title = "Star Wars: Episode VII - The Force Awakens",
                    ReleaseYear = 2015,
                    Rating = 8.4m
                },
                new Movie()
                {
                    Title = "Star Wars: Episode III - Revenge of the Sith",
                    ReleaseYear = 2005,
                    Rating = 7.6m
                },
                new Movie()
                {
                    Title = "Star Wars: Episode VI - Return of the Jedi",
                    ReleaseYear = 1983,
                    Rating = 8.4m
                },
                new Movie()
                {
                    Title = "Star Wars: The Last Jedi",
                    ReleaseYear = 2017,
                    Rating = 0
                },
                new Movie
                {
                    Title = "Rogue One",
                    ReleaseYear = 2016,
                    Rating = 7.9m
                },
                new Movie
                {
                    Title = "Blade Runner",
                    ReleaseYear = 1982,
                    Rating = 8.2m
                }
            };
        }
    }
}
