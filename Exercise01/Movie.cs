﻿using CollectionView;
using System;
using System.Collections.Generic;

namespace CollectionViewTest
{
    public class Movie
    {
        public string Title { get; set; }
        public decimal Rating { get; set; }
        public int ReleaseYear { get; set; }
    }

}
